# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2023-05-01

### Changed

- Updated dependencies

## [0.2.0] - 2019-12-13

### Changed

- Updated dependencies

## [0.1.0] - 2019-11-19

### Added

- Initial Release

[0.1.0]: https://gitlab.com/horse-racing-labs/buffer-api/releases/tag/v0.1.0

