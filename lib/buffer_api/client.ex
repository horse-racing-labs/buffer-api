defmodule BufferApi.Client do
  @moduledoc """
  Client is used to pass configuration to the buffer api.
  When making an API call you will need to first grab a client 
  using `BufferApi.Client.from_system/0` or `BufferApi.Client.from_application/1`.

  Which call you use will be determined by which method of configuration you use
  """

  defstruct access_token: "", api_url: ""

  @doc """
  Returns default BufferApi client to be passed to api functions. 
  For from_application/0 to work the following config is expected:

    buffer_access_token: ...,
    buffer_api_url: ...

  ## Examples
    iex> BufferApi.Client.from_application()
    %BufferAPI.Client{...}
  """
  def from_application(app) do
    %BufferApi.Client{
      access_token: Application.get_env(app, :buffer_access_token),
      api_url: Application.get_env(app, :buffer_api_url)
    }
  end

  @doc """
  Returns default BufferApi client to be passed to api functions. 
  For from_system/0 to work the following environment variables are expected:

    BUFFER_ACCESS_TOKEN
    BUFFER_API_URL

  ## Examples
    iex> BufferApi.Client.from_application()
    %BufferAPI.Client{...}
  """
  def from_system() do
    %BufferApi.Client{
      access_token: System.get_env("BUFFER_ACCESS_TOKEN"),
      api_url: System.get_env("BUFFER_API_URL")
    }
  end
end
