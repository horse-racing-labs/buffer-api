defmodule BufferAPI.Profile do
  defstruct verb: "",
            preferences: %{},
            is_analyze_enabled: false,
            team_members: [],
            timezone_city: "",
            disabled: false,
            locked: false,
            service_username: "",
            timezone: "",
            id: "",
            formatted_service: "",
            paused_schedules: [],
            user_id: "",
            can_see_content_library: false,
            reports_logo: nil,
            disabled_features: [],
            statistics: [],
            shortener: %{},
            cover_photo: "",
            formatted_username: "",
            _id: "",
            service_type: "",
            utm_tracking: "",
            service: "",
            created_at: 0,
            avatar_https: "",
            disconnected: false,
            default: true,
            schedules: [],
            is_on_business_v2: false,
            paused: false,
            avatar: "",
            counts: %{},
            service_id: ""
end
