defmodule BufferAPI.User do
  defstruct activity_at: 0,
            billing_plan_base: "",
            billing_plan_tier: "",
            billing_status_nonprofit: false,
            created_at: 0,
            id: "",
            pablo_preferences: [],
            plan: "",
            profile_groups: [],
            timezone: "",
            twentyfour_hour_time: false,
            week_starts_monday: false
end
