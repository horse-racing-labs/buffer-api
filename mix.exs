defmodule BufferApi.MixProject do
  use Mix.Project

  def project do
    [
      app: :buffer_api,
      version: "0.3.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.29", only: :dev, runtime: false},
      {:httpoison, "~> 2.1"},
      {:jason, "~> 1.4"}
    ]
  end

  defp description do
    """
      Wrapper Library for [The Buffer API](https://buffer.com/developers/api)
    """
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "LICENSE*", "CHANGELOG*"],
      maintainers: ["Matthew Caldwell"],
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/horse-racing-labs/buffer-api"},
      source_url: "https://gitlab.com/horse-racing-labs/buffer-api",
      homepage_url: "https://gitlab.com/horse-racing-labs/buffer-api"
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["CHANGELOG.md", "README.md"]
    ]
  end
end
